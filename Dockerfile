# set base image (host OS)
FROM python:3.8

# set the working directory in the container
WORKDIR /code

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY src/ .

# command to run on container start
CMD [ "python", "./backend.py" ]