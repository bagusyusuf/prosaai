<div align="center" id="top"> 
  <!-- <img src="./.github/app.gif" alt="Prosaai" /> -->

  &#xa0;

  
</div>

<h1 align="center">Prosaai</h1>

<!-- <p align="center">
  <img alt="Github top language" src="https://img.shields.io/github/languages/top/{{YOUR_GITHUB_USERNAME}}/prosaai?color=56BEB8">

  <img alt="Github language count" src="https://img.shields.io/github/languages/count/{{YOUR_GITHUB_USERNAME}}/prosaai?color=56BEB8">

  <img alt="Repository size" src="https://img.shields.io/github/repo-size/{{YOUR_GITHUB_USERNAME}}/prosaai?color=56BEB8">

  <img alt="License" src="https://img.shields.io/github/license/{{YOUR_GITHUB_USERNAME}}/prosaai?color=56BEB8">

  <!-- <img alt="Github issues" src="https://img.shields.io/github/issues/{{YOUR_GITHUB_USERNAME}}/prosaai?color=56BEB8" /> -->

  <!-- <img alt="Github forks" src="https://img.shields.io/github/forks/{{YOUR_GITHUB_USERNAME}}/prosaai?color=56BEB8" /> -->

  <!-- <img alt="Github stars" src="https://img.shields.io/github/stars/{{YOUR_GITHUB_USERNAME}}/prosaai?color=56BEB8" /> -->
<!-- </p> --> -->

<!-- Status -->

<!-- <h4 align="center"> 
	🚧  Prosaai 🚀 Under construction...  🚧
</h4> 

<hr> -->

<!-- <p align="center">
  <a href="#dart-about">About</a> &#xa0; | &#xa0; 
  <a href="#sparkles-features">Features</a> &#xa0; | &#xa0;
  <a href="#rocket-technologies">Technologies</a> &#xa0; | &#xa0;
  <a href="#white_check_mark-requirements">Requirements</a> &#xa0; | &#xa0;
  <a href="#checkered_flag-starting">Starting</a> &#xa0; | &#xa0;
  <a href="#memo-license">License</a> &#xa0; | &#xa0;
  <a href="https://github.com/{{YOUR_GITHUB_USERNAME}}" target="_blank">Author</a>
</p> -->

<br>

## :dart: About ##

This is simple chatbot test from prosa ai, developed to fulfill the recrutment process

## :sparkles: Features ##

:heavy_check_mark: Question answer pair;\
:heavy_check_mark: Docker env ready;

## :rocket: Technologies ##

The following tools were used in this project:

- [Python3]
- [Docker]
- [Flask]
- [Nltk](https://www.nltk.org/)
- [Javascript]

## :white_check_mark: Requirements ##

Before starting :checkered_flag:, you need to have [Git](https://git-scm.com) and [Docker](https://docker.com) installed.

## :checkered_flag: Starting ##

```bash
# Clone this project
$ git clone https://github.com/bagusyusuf/prosaai

# Access
$ cd prosaai

# build docker
$ Docker build -t {label} .

# Run the project
$ Docker run -d -p 5000:5000 {label}

# The server will initialize in the <http://localhost:5000>
```

## :checkered_flag: Configuration ##

This chatbot will only reply question in the src/list_word,
if you want to add another question answer pair please use this format,
```
question : answer11,answer2,answer3
```
if you want to have several answer for one question please add " , " to separate the question.
if you want to recieve more than one question with the same answer please add " | " to separate the question
```
# ex.
(hi|hello|hey|hola): hey there,hi,hi there
```

## :checkered_flag: Sample ##
![alt text](https://gitlab.com/bagusyusuf/prosaai/-/raw/master/captured.gif)

&#xa0;

<a href="#top">Back to top</a>
