document.addEventListener("DOMContentLoaded", () => {
    const inputField = document.getElementById("input")
    inputField.addEventListener("keydown", function (e) {
        if (e.code === "Enter") {
            let input = inputField.value;
            inputField.value = "";
            getResultMsg_("reply", input).then(data => output(input, data['reply-msg']));
        }
    });
});

function initMsg() {
    getResultMsg("init").then(data => initChat(data['wlcome-msg']));
}

function initChat(data) {
    const mainDiv = document.getElementById("bagus");

    let botDiv = document.createElement("div");
    botDiv.id = "bot";
    botDiv.innerHTML = `Chatbot: <span id="bot-response">${data}</span>`;
    mainDiv.appendChild(botDiv);

    var atbottom = scrollAtBottom(mainDiv);
    console.log(atbottom)
    if (!atbottom) {
        updateScroll(mainDiv);
    }
}

function output(input, product) {
    let text = input.toLowerCase().replace(/[^\w\s\d]/gi, "");

    //update DOM
    addChat(input, product);
}


function addChat(input, product) {
    const mainDiv = document.getElementById("bagus");

    let userDiv = document.createElement("div");
    userDiv.id = "user";
    userDiv.innerHTML = `You: <span id="user-response">${input}</span>`;
    mainDiv.appendChild(userDiv);

    let botDiv = document.createElement("div");
    botDiv.id = "bot";
    botDiv.innerHTML = `Chatbot: <span id="bot-response">${product}</span>`;
    mainDiv.appendChild(botDiv);

    var atbottom = scrollAtBottom(mainDiv);
    console.log(atbottom)
    if (!atbottom) {
        updateScroll(mainDiv);
    }
}

// scroll to bottom
function updateScroll(el) {
    el.scrollTop = el.scrollHeight;
}

function scrollAtBottom(el) {
    return (el.scrollTop + 5 >= (el.scrollHeight - el.offsetHeight));
}

// get the reply from backend
async function getResultMsg(url_sp) {

    let url = "http://localhost:5000/chatbot/"
    let resp = await fetch(url + url_sp);
    let data = await resp.json();
    // console.log(data);
    return data;
}



async function getResultMsg_(url_sp, msg) {
    let url = "http://localhost:5000/chatbot/"

    let resp = await fetch(url + url_sp, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({ "msg": msg })
    });
    let data = await resp.json();
    return data;
}