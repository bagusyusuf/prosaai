from flask import Flask, json, request, jsonify, render_template
from flask_cors import CORS
from nltk.chat.util import Chat, reflections

pairs = []
with open("list_word.txt") as f:
    for line in f:
        mylist = line.strip().split(':')
        pairs.append([mylist[0], mylist[1].strip().split(',')])

# native python dictionary key - pair
# this is not robust but it can be used to find key - value pair
# use user input as key and value as replies
key_value = dict()
with open("testnonnltk.txt") as f:
    for line in f:
        mylist = line.strip().split(':')
        key_value[mylist[0]]= mylist[1]


api = Flask(__name__)
chat = Chat(pairs, reflections)
CORS(api)

# Route API

@api.route('/')
def home():
    return render_template("index.html")

@api.route('/chatbot/init', methods=['GET'])
def send_init_msg():
    return json.dumps({ "wlcome-msg": "hi can i help you"})

@api.route('/chatbot/reply', methods=['POST'])
def get_reply():

    # get the body from json request
    body = request.json.get('msg', None)
    
    # since i only need to find the pair 
    # i can use the key - pair in python dictionary
    # but i am using the nltk package to make it more robust
    rsp = chat.respond(body)

    # use the code below if i am not allowed to use nltk
    # rsp = key_value.get(body,None)

    if rsp == None:
        rsp = "i dont know how to response"

    # print(rsp)
    return json.dumps({"reply-msg": rsp})

if __name__ == '__main__':
    api.run(host='0.0.0.0')